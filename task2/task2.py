import json
import os, logging, uuid, boto3, ffmpy
from datetime import datetime

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# VID_BUCKET= "aayush-karki-videos"
THUMBNAIL_BUCKET= "aayush-karki-thumbnails"



# s3 = boto3.resource('s3')
s3 = boto3.client('s3')


def download(filename_ext, VIDEO_DIR, THUMB_DIR, VID_BUCKET):
    logger.debug("output: "+THUMB_DIR)
    logger.debug("input: "+VIDEO_DIR)
    logger.debug("vid bucket: "+VID_BUCKET)
    logger.debug("thumbnail bucket: "+THUMBNAIL_BUCKET)

    logger.debug("/tmp before mkdirds "+str(os.listdir('/tmp')))
    if(os.system("mkdir %s" % VIDEO_DIR)!=0):
        logger.error("unable to mkdir %s" %VIDEO_DIR)
        return False, 1
    if(os.system("mkdir %s" % THUMB_DIR)!=0):
        logger.error("unable to mkdir %s" %THUMB_DIR)
        return False, 2
    logger.debug("/tmp after mkdirds "+str(os.listdir('/tmp')))
    video_local_path = VIDEO_DIR+'/'+filename_ext
    logger.debug("video local path: "+video_local_path)

    try:
        logger.info("downloading %s %s" %(VID_BUCKET,filename_ext))
        s3.download_file(VID_BUCKET, filename_ext, video_local_path)
    except Exception as e:
        logger.error("s3 download failed", exc_info=True)
        return False, 3

    logger.debug("vid local path: %s" %video_local_path)
    return True, video_local_path

def create_thumbnails(video_local_path, THUMB_DIR, VIDEO_DIR):
    global THUMBNAIL_BUCKET
    t_start = datetime.utcnow()
    filename = video_local_path.split("/")[-1].split(".")[0]
    logger.debug("filename: %s" %filename)

    ffmpeg_path = "%s/ffmpeg" %VIDEO_DIR
    if os.system("cp ffmpeg %s" %ffmpeg_path)!=0:
        logging.error("could not copy ffmpeg to %s" %ffmpeg_path)
    if os.system("chmod 755 %s" %ffmpeg_path)!=0:
        logging.error("could not chmod 755 to %s" %ffmpeg_path)

    output_path_frmt = THUMB_DIR+ "/" + filename + "_%d.png"
    f = ffmpy.FFmpeg(
        executable=ffmpeg_path,
        inputs={video_local_path: None},
        outputs={output_path_frmt: '-y -vf fps=1 -loglevel error'}
    )
    try:
        logger.debug("ffmpeg cmd: %s" %f.cmd)
        f.run()
        logger.debug("ffmpeg ran well")
        t_end = datetime.utcnow()
        t_secs = (t_end - t_start).total_seconds()
        logger.info("thumb gen secs: %s" % t_secs)
    except:
        logger.error("ffmpeg raised error", exc_info=True)
        return False, 4

    thumb_names = os.listdir(THUMB_DIR)
    thumb_names.sort()
    logger.info("thumbnames: " + str(thumb_names) )

    t_start = datetime.utcnow()
    for name in thumb_names:
        path = "%s/%s" %(THUMB_DIR,name)
        try:
            s3.upload_file(path, THUMBNAIL_BUCKET, name)
            logger.info("upload detail: %s %s %s" %(path, THUMBNAIL_BUCKET, name))
        except Exception as e:
            logger.error("s3 upload failed", exc_info=True)
            return False, 5
    t_end = datetime.utcnow()
    logger.debug("uploading all thumbnails successful")
    t_secs = (t_end - t_start).total_seconds()
    logger.info("thumbs upload secs: %s" % t_secs)
    return True, 0


def handle(event, context):
    result = {'error_keys': [], 'total':0}
    for record in event['Records']:
        for sub_record in json.loads(record['Sns']['Message'])['Records']:
            result['total'] += 1
            VID_BUCKET =  sub_record['s3']['bucket']['name']
            filename_ext = sub_record['s3']['object']['key']
            dir = str(uuid.uuid4())[:7]
            VIDEO_DIR = "/tmp/" + dir + "-input"
            THUMB_DIR = "/tmp/" + dir + "-output"
            try:
                succ, filename_ext = download(filename_ext, VIDEO_DIR, THUMB_DIR, VID_BUCKET)
                if succ:
                    create_thumbnails(filename_ext, THUMB_DIR=THUMB_DIR, VIDEO_DIR=VIDEO_DIR)
            except:
                result['error_keys'].append(filename_ext)
            finally:
                pass
                # if os.system("rm -rf %s" % VIDEO_DIR) ==0:
                #     logger.debug("removed: %s" %VIDEO_DIR)
                # else:
                #     logger.error("could not delete %s" %VIDEO_DIR)
                #
                # if os.system("rm -rf %s" % THUMB_DIR) == 0:
                #     logger.debug("removed: %s" % THUMB_DIR)
                # else:
                #     logger.error("could not delete %s" % THUMB_DIR)
    return result
