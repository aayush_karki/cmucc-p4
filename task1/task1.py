import ipaddress
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def cidrStats(event, context):
    try:
        logging.info("cidrStats invoked")
        cidrSignature=event['cidrSignature']
        net = ipaddress.IPv4Network(cidrSignature, strict=False)
        first = str(net[0])
        last = str(net[-1])
        count = net.num_addresses
        response={
            "cidrSignature": cidrSignature,
            "firstAddress": first,
            "lastAddress": last,
            "addressCount": count
        }
        logging.info("Cidr stats returning successfully")
    except Exception as e:
        logging.error("Error CidrStats", exc_info=True)
        response={
        }
    finally:
        logging.info("Output: %s" %response.__str__())
        return response


if __name__=='__main__':
    res = cidrStats({"cidrSignature": "172.10.242.81/12"}, {})
    assert('172.10.242.81/12'==res['cidrSignature'])
    assert('172.0.0.0'==res['firstAddress'])
    assert('172.15.255.255'==res['lastAddress'])
    assert(1048576==res['addressCount'])




